/**
 * @title 武汉洪灾历史记录
 * @des 控制器类
 * @author sz
 * @date 2016-07-08
 * @version 1.0
 */

var que;
Ext.define(
    'KPSFT.wfore.controller.WuHan_History',
    {
        extend : 'KPSFT.frmwk.controller.Base',
        _GET_INIT_ACT : function() {
            var date = new Date(), hours = date.getHours();
            if (hours < 8) {
                date = Ext.Date.add(date, Ext.Date.DAY, -1);
            }
            date = Ext.Date.format(date, 'Y-m-d 08:00');
            this.view.down('datetimefield[name=date]').setValue(
                date);
            return [ {
                JSCODE : G_LOAD,
                CODE : 'BBB_SEL',
                RCODE : 'SEL',
                CDN : {
                    date : date
                }
            } ];
        },

        _LOAD_INIT_DATA : function(data) {
            this.gridStore.loadData(data.SEL);
            this._data = data.SEL;
            que = this;
        },
        // 响应事件
        _GET_ADD_EVENTS : function() {
            return {
                'button[action=add_btn]' : {
                    click : this.openWin
                },
                'button[action=query_btn]' : {
                    click : this.doQuery
                },
                'button[action=exportExcel]' : {
                    click : this.exportExcel
                }
            }
        },
        openWin : function() {
            var gcWin = _APP.viewport.down('window[name=gcWin]');
            if (!gcWin) {
                gcWin = Ext
                    .create(
                    'Ext.window.Window',
                    {
                        title : '增加地点',
                        width : 450,
                        height : 500,
                        modal : true,
                        name : 'gcWin',
                        layout : 'fit',
                        constrain : true,
                        items : [ {
                            xtype : 'form',
                            defaults : {
                                xtype : 'textfield',
                                anchor : '100%',
                                labelWidth : 70
                            },
                            border : false,
                            bodyPadding : 10,
                            items : [
                                {
                                    xtype : 'hidden',
                                    name : 'idx'
                                },
                                {
                                    xtype : "textfield",
                                    id : 'ID',
                                    fieldLabel : '站点代号',
                                    name : 'ID',
                                    allowBlank : false,
                                    allowOnlyWhitespace : false
                                },
                                {
                                    fieldLabel : '站点名称',
                                    id : 'Name',
                                    name : 'Name',
                                    allowBlank : false,
                                    allowOnlyWhitespace : false
                                    // ,value:'SOURCECODE'
                                },
                                {
                                    fieldLabel : '当日降雨量',
                                    id : 'Today_J',
                                    name : 'Today_J',
                                    allowBlank : false,
                                    allowOnlyWhitespace : false
                                    // ,value:this.SOURCENAME
                                },
                                {
                                    fieldLabel : '当日蓄水量',
                                    id : 'Today_X',
                                    name : 'Today_X',
                                    allowBlank : false,
                                    allowOnlyWhitespace : false
                                    // ,value:this.SOURCENAME
                                },
                                {
                                    fieldLabel : '历史降雨量',
                                    id : 'History_J',
                                    name : 'History_J',
                                    allowBlank : false,
                                    allowOnlyWhitespace : false
                                    // ,value:this.SOURCENAME
                                },
                                {
                                    fieldLabel : '历史蓄水量',
                                    id : 'History_X',
                                    name : 'History_X',
                                    allowBlank : false,
                                    allowOnlyWhitespace : false
                                    // ,value:this.SOURCENAME
                                } ],
                            dockedItems : [ {
                                xtype : 'toolbar',
                                dock : 'bottom',
                                items : [
                                    {
                                        text : '保存',
                                        handler : function() {
                                            var form = _APP.viewport
                                                .down('form');
                                            if (!form
                                                    .isValid())
                                                return KPSFT.pop
                                                    .msg(
                                                    '提示',
                                                    '请填写完整！');
                                            var param = _APP.viewport
                                                .down(
                                                'form')
                                                .getVals();
                                            var code = G_UPDATE;
                                            if (!param.idx)
                                                code = G_INSERT;
                                            // var
                                            // step=[{JSCODE:G_SAVE,CODE:code,RCODE:'DATA',DATA:_RM_N_P(param),CLAZZ:'[pwhd].[dbo].[YW_STCD_LIFE_POLLUTION_SOURCE]'}];

                                            var step = [ {
                                                JSCODE : G_SAVE,
                                                CODE : code,
                                                RCODE : 'DATA',
                                                DATA : _RM_N_P(param),
                                                CLAZZ : 'BBB',
                                                SCRIPT_CODE : 'BBB'
                                            } ];
                                            __EXC_STEP(
                                                step,
                                                this,
                                                function(
                                                    data,
                                                    response) {
                                                    KPSFT.pop
                                                        .msg(
                                                        data.title,
                                                        data.content);
                                                    var gcWin = _APP.viewport
                                                        .down('window[name=gcWin]');
                                                    gcWin
                                                        .hide();
                                                    que
                                                        .doQuery();
                                                    form
                                                        .reset();
                                                });
                                            alert("提交！")
                                        }
                                    },
                                    {
                                        text : '取消',
                                        handler : function() {

                                            gcWin
                                                .hide();
                                        }
                                    } ]
                            } ]
                        } ]
                    });
                _APP.viewport.add(gcWin);
                gcWin.show();
            }
            gcWin.show();
        },
        doQuery : function() {
            var date = this.view.down('datetimefield[name=date]')
                .getValue();
            var step = [ {
                JSCODE : G_LOAD,
                CODE : 'BBB_SEL',
                RCODE : 'SEL',
                CDN : {
                    date : date
                }
            } ];
            __EXC_STEP(step, this, function(data, response) {
                this._LOAD_INIT_DATA(data.detail);
            });
        },
        // 导出功能
        exportExcel : function() {
            var myMask = new Ext.LoadMask({
                msg : '请稍后...',
                target : this.up('window')
            }).show();
            Ext.Ajax.request({
                url : 'excelTpl.action',
                scope : this,
                jsonData : this._data,
                success : function(response) {
                    myMask.destroy();
                    Ext.Msg.alert('错误', '导出成功！');
                    // window.location.href='download/SewageInfo.xls';
                },
                failure : function(response) {
                    myMask.destroy();
                    Ext.Msg.alert('错误', '导出失败！');
                },
            });
        }
    });

function deleteInfo(value) {
    Ext.Msg.confirm('删除确认', '确定要删除该记录？', function(cbtn) {
        if (cbtn != 'yes')
            return;
        var odata = {
            ID : value
        };
        // var ID = parseInt(odata);
        var step = [ {
            JSCODE : G_SAVE,
            CODE : G_DELETE,
            DATA : odata,
            RCODE : 'SEL',
            CLAZZ : 'BBB'
        } ];
        // 向后台发送保存数据，同时获取返回结果
        __EXC_STEP(step, this, function(data, response) {
            KPSFT.pop.msg(data.title, data.content);
            //
            this.que.doQuery();
            // this.doQuery();
            // window.parent.doQuery();
        });
        // gridStore.load();
    }, this);
}
function modifyInfo(value) {
    var gbWin = _APP.viewport.down('window[name=gbWin]');
    var param = {};
    param.ID = value;
    var step = [ {
        JSCODE : G_LOAD,
        CODE : 'BBB_SEL_ONE',
        RCODE : 'LPS',
        CDN : param
    } ];
    __EXC_STEP(step, this, function(data, response) {
        var rsData = data.detail.LPS[0];
        if (!gbWin) {
            gbWin = Ext.create('Ext.window.Window', {
                title : '修改数据',
                width : 450,
                height : 500,
                modal : true,
                name : 'gbWin',
                layout : 'fit',
                constrain : true,
                items : [ Ext.create("Ext.form.Panel", {
                    renderTo : Ext.getBody(),
                    defaultType : 'textfield',
                    defaults : {
                        anchor : '100%',
                    },
                    fieldDefaults : {
                        labelWidth : 80,
                        labelAlign : "left",
                        flex : 1,
                        margin : 5
                    },
                    items : [ {
                        xtype : "container",
                        layout : "hbox",
                        items : [ {
                            xtype : "textfield",
                            name : "ID",
                            fieldLabel : "站点代号",
                            allowBlank : false,
                            value : rsData.ID,
                            readOnly : true
                        }, ]
                    }, {
                        xtype : "container",
                        layout : "hbox",
                        items : [ {
                            xtype : "textfield",
                            name : "Name",
                            fieldLabel : "站点名称",
                            allowBlank : false,
                            emptyText : "",
                            value : rsData.Name
                        }, ]
                    }, {
                        xtype : "container",
                        layout : "hbox",
                        items : [ {
                            xtype : "textfield",
                            name : "Today_J",
                            fieldLabel : "当日降雨量",
                            allowBlank : false,
                            emptyText : "",
                            value : rsData.Today_J
                        }, ]
                    }, {
                        xtype : "container",
                        layout : "hbox",
                        items : [ {
                            xtype : "textfield",
                            name : "Today_X",
                            fieldLabel : "当日蓄水量",
                            allowBlank : false,
                            emptyText : "",
                            value : rsData.Today_X
                        }, ]
                    } ],
                    buttons : [ {
                        xtype : "button",
                        text : "保存",
                        handler : function() {
                            var ID = this.up('form').getForm().findField('ID')
                                .getValue();
                            var Name = this.up('form').getForm().findField(
                                'Name').getValue();
                            var Today_J = this.up('form').getForm().findField(
                                'Today_J').getValue();
                            var Today_X = this.up('form').getForm().findField(
                                'Today_X').getValue();
                            var param = {
                                ID : ID,
                                Name : Name,
                                Today_J : Today_J,
                                Today_X : Today_X
                            };
                            var code = G_UPDATE;
                            var step = [ {
                                JSCODE : G_UPDATE,
                                CODE : code,
                                RCODE : 'DATA',
                                DATA : param,
                                CLAZZ : 'BBB',
                                SCRIPT_CODE : 'BBB'
                            } ];
                            __EXC_STEP(step, this, function(data, response) {
                                KPSFT.pop.msg(data.title, data.content);
                                var gbWin = _APP.viewport
                                    .down('window[name=gbWin]');
                                gbWin.hide();
                                __EXC_STEP(step, this,
                                    function(data, response) {
                                        KPSFT.pop.msg(data.title,
                                            data.content);
                                        // _APP.viewport.down('window[name=grid]').doQuery();
                                        // window.parent.doQuery();
                                        que.doQuery();
                                    });
                            });
                        }
                    } ]
                })

                ]
            });
            _APP.viewport.add(gbWin);
            gbWin.show();
        }
        gbWin.show();
    });
}

/**
 * @title 武汉历史洪灾
 * @des 视图类
 * @author sz
 * @date 2016-07-08
 * @version 1.0
 */
Ext
    .define(
    'KPSFT.wfore.view.WuHan_History',
    {
        extend : 'KPSFT.frmwk.view.Base',
        layout : 'fit',
        crtLayout : function() {
            this.items = [ this.crtCnt() ];
        },

        crtCnt : function() {
            return {
                xtype : 'grid',
                store : this.gridStore,
                columns : [
                    // { xtype:'rownumberer' },
                    {
                        text : '站点代号',
                        dataIndex : 'ID',
                        width : 120,
                        align : 'center',
                        menuDisabled : true,
                        emptyCellText : '-'
                    },
                    {
                        text : '站点名称',
                        dataIndex : 'Name',
                        width : 120,
                        align : 'center',
                        menuDisabled : true,
                        emptyCellText : '-'
                    },
                    {
                        text : '今日降雨量(mm)',
                        dataIndex : 'Today_J',
                        width : 120,
                        align : 'center',
                        menuDisabled : false,
                        emptyCellText : '-'
                    },
                    {
                        text : '今日蓄水量(万m3)',
                        dataIndex : 'Today_X',
                        width : 120,
                        align : 'center',
                        menuDisabled : false,
                        emptyCellText : '-'
                    },
                    {
                        text : '历史降雨量(mm)',
                        dataIndex : 'History_J',
                        width : 120,
                        align : 'center',
                        menuDisabled : false,
                        emptyCellText : '-'
                    },
                    {
                        text : '历史蓄水量(万m3)',
                        dataIndex : 'History_X',
                        width : 120,
                        align : 'center',
                        menuDisabled : false,
                        emptyCellText : '-'
                    },
                    {
                        text : '操作',
                        width : 120,
                        align : 'center',
                        menuDisabled : true,
                        dataIndex : 'ID',
                        emptyCellText : '-',
                        renderer : function(value) {
                            return '<input type="button" onclick="deleteInfo(\''
                                + value
                                + '\');" value="删除"/>&nbsp;<input type="button" onclick="modifyInfo(\''
                                + value
                                + '\');" value="修改"/>'

                        }
                    } ],
                tbar : [ {
                    xtype : 'datetimefield',
                    fieldLabel : '日期',
                    labelWidth : 30,
                    name : 'date',
                    dateFmt : 'yyyy-MM-dd 08:00',
                    maxDate : '%y-%M-%d'
                }, {
                    action : 'query_btn',
                    text : '查询'
                }, {
                    action : 'add_btn',
                    text : '增加站点'
                }, '->', {
                    action : 'exportExcel',
                    text : '导出',
                    icon : 'resources/images/newBtn/out-ico.png'
                } ]
            };
        }
    });

/**
 * @title 武汉历史洪灾
 * @des 存储
 * @author sz
 * @date 2016-07-08
 * @version 1.0
 */
Ext.define('KPSFT.wfore.store.WuHan_History', {
    extend : 'KPSFT.frmwk.store.Base',
    createStore : function() {
        this.sl = {
            gridStore : Ext.create('Ext.data.Store', {
                fields : []
            })
        };
    }
});
